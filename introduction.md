# WhiteBoardCanvas

## Introduction

### Key Requirements of White Board Canvas

- Multiple Pages

- Multiple Layers on Each page

- Free Hand writing on canvas

- Support Embedding images, iframes and other HTML attribute

- Anotation over embeded elements

- Navigation on Embeded iframes

- Undo Redo for every action performed by the users

- Geomeatric Zooming and mobile Friendly fit

### Dependencies of for fullfilment of Requirement

| Requirement       |                                            Dependencies |
| ----------------- | ------------------------------------------------------: |
| Multiple Pages    |                                              Navigation |
| Multiple Layers   | Write Mode, Select Mode, Layer Tracing, Layer Insertion |
| Free Hand Writing |                                              Write Mode |
| Embedding         |                Write Mode, Layer Insertion, Select Mode |
| Anotation         |                Write Mode, Layer Insertion, Select Mode |
| Iframe Navigation |                                             Select Mode |
| Undo Redo         |         Action collection, Storage Struct, Storage Sync |
| Iframe Navigation |                                             Select Mode |

### Layers / Strokes

A layers will have set of storkes (mostly lines), these set of strokes will treated as an Element.

### Element

Elements can be an individual 'image', 'iframe', 'video' or a set of strokes.

### How do strokes work?

- There is always a default canvas above all the elements. This canvas will cover the whole screen and z-index as `1`, to catch actions of user's stylus.

- `ontouchstart` : Recording strokes is initated

- `ontouchmove` : New Strokes are added to recording

- `ontouchend` : The Last Stroke is Recorded, min-max extents of X & Y coordinates are identified and the Recorded stores are added to elements Array.

- The canvas is resised to min-max extents of X & Y coordinates, and z-index of this set to `0`

- New Canvas with z-index `1` is added to cover the screen and catch actions of user's stylus.

### Page

A Page is collection Layers. This Page can perform Geometric Zooming.

### Geometric Zooming

- Perform Zooming with HTML CSS zoom

- All Layers are stored as individual elements and zoom ratio is set accordingly.